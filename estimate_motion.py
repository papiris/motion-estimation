#!/bin/env python3

#---------------------------#
# Copyright (c) 2022 Jacob Dybvald Ludvigsen (jd_lud at pm dot me)
#--------------------------#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3.0
# as published by the Free Software Foundation and appearing in the file
# LICENSE.txt included in the directory of this file.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#--------------------------#

#install dependencies:
# python3 -m pip install numpy rawpy matplotlib opencv-contrib-python h5py matplotlib

#import sklearn # eliminating outliers in dense optical flow
from sklearn.metrics import mean_absolute_percentage_error, mean_absolute_error
from scipy import interpolate
from scipy.stats import spearmanr
#import h264decoder # to directly open .h264 files
import csv # for output of data
import h5py # to enable high-performance file handling
#from numba import jit, njit # to compile code for quicker execution
from multiprocessing import Pool # to run multiple instances of time-consuming processes
import rawpy
import rawpy.enhance
import subprocess # to execute c-program "double"
import linecache # to read long files line by line efficiently
import random # to choose a random image from image range
import numpy as np # to manipulate images as arrays
import argparse # to accept command-line input
import cv2 as cv # to use various image manipulations
import matplotlib.pyplot as plt # for plot functionality
from pathlib import Path # to handle directory paths properly
#from memory_profiler import profile # for memory benchmarking
import time

start_time = time.time()


### Get input
def retFileList():
    numberList = []
    firstFrame = ''
    lastFrame = ''
    parser = argparse.ArgumentParser()
    parser.add_argument(type=int, nargs ='*', action='store', dest='fileIndex', \
          default='False', help='index of first image to be read. If full is passed, the whole video is used')

    parser.add_argument('-f', '--full', nargs ='?', action='store', dest='useWholeVideo', \
          default='False', const='True', help='index of last image to be read. If full is passed, the whole video is used')

    parser.add_argument('-p', '--path', nargs='?', type=Path, dest="srcDirectory", default='/dev/shm/', \
          help='which directory to read images from. Specify with "-p <path-to-folder>" or "--path <path-to-folder". Leave empty for /dev/shm/')

    parser.add_argument('-c', '--continue', nargs='?', action='store', dest='continuation', \
        default='False', const='True', help='continue analysis of video from previous attempt')

    parser.add_argument('-h264', nargs='?', action='store', dest='h264',
                        default='False', const='True', help='whether to read h264 file directly instead of mkv file.')

    parser.add_argument('-d', '--denoise', nargs='?', action='store', dest="_denoise", default='False', \
          const='True', help='whether to denoise video')

    parser.add_argument('-r', '--raw', nargs='?', action='store', dest="_raw", default='False', \
          const='True', help='whether to read raw frames instead of video as input.')
    args = parser.parse_args()
    srcDir = args.srcDirectory
    imagePath = str(srcDir)

    continuation = args.continuation

    if args.fileIndex != None and args.useWholeVideo != 'True':
        firstFrame, lastFrame = args.fileIndex
        r = range(firstFrame, lastFrame)
        numberList = list([*r])
        numberList.append(lastFrame)
    if args.useWholeVideo == "True":
        lastFrame = -1


    return firstFrame, lastFrame, numberList, imagePath, continuation, args.h264, args._denoise, args._raw



firstFrame, lastFrame, numberList, imagePath, continuation, use_h264, use_denoise, use_raw = retFileList()


### get video metadata for regular video
def get_meta():
    for fileName in Path(imagePath).glob("*.mkv"):
        vid_file = str(fileName)
        break

    cap = cv.VideoCapture(vid_file)
    # get vcap property
    width  = int(cap.get(3))   # float `width`
    height = int(cap.get(4))  # float `height`
    totalFrames = int(cap.get(7)) # cv.CAP_PROP_FRAME_COUNT
    cap.release()

    return width, height, totalFrames, vid_file


"""
### get video metadata for h264 video. currently broken
def get_meta_h264():
    for fileName in Path(imagePath).glob("*.h264"):
        vid = open(fileName, 'rb')
        decoder = h264decoder.H264Decoder()
        while (1):
            data_in = vid.read(1024)
            if not data_in:
                break
            framedata, nread = decoder.decode_frame(data_in)
            data_in = data_in[nread:]
            (frame, width, height, lineSize) = framedata
            break
        break

    return width, height
"""

if use_h264 != "True" and use_raw != "True":
    width, height, totalFrames, vid_file_name = get_meta()
elif use_h264 == "True":
    width, height = get_meta_h264()


if lastFrame == -1:
    firstFrame = 0
    lastFrame = totalFrames
    r = range(firstFrame, lastFrame)
    numberList = list([*r])
    numberList.append(lastFrame)



# prepend headers to rawfiles if they don't already have a header
def checkRawHeader():
    hf = open('./examples/raws/hd0.32k', 'rb')
    header = hf.read()
    hf.close()
    #fileListMap = map(str, numberList)
    fileList = [str(x).zfill(4) for x in list(numberList)]
    fileList = [str(imagePath)+"/out."+str(x)+".raw" for x in list(fileList)]
    for x in list(fileList):
        with open(x, 'rb') as rawFile: partialRaw = rawFile.read(32) # read first 32 blocks of raw
        if header != partialRaw: # check whether the first 32 blocks of the rawfile is identical to the header
            with open(x, 'rb') as original: data = original.read()
            with open(x, 'wb') as modified: modified.write(header + data)
    return fileList


def readRaws(headedList):

    for n, x in enumerate(headedList):
    # incoming data
        with rawpy.imread(x) as raw:
            if n >= 2 and n+2 <= len(headedList):
                raw_enhance_list = headedList[n-2:n+2]
                raw_bad_pix = rawpy.enhance.find_bad_pixels(
                        raw_enhance_list,
                        find_hot=True,
                        find_dead=True,
                        confirm_ratio=0.8)

                rawpy.enhance.repair_bad_pixels(raw, raw_bad_pix, method='median')
            rgb = raw.postprocess(
                      fbdd_noise_reduction=rawpy.FBDDNoiseReductionMode.Full,
                      demosaic_algorithm=rawpy.DemosaicAlgorithm(4),
                      dcb_iterations=7,
                      dcb_enhance=True,
                      median_filter_passes=5,
                      use_auto_wb=True,
                      output_bps=8)

            grayframe = cv.cvtColor(rgb, cv.COLOR_BGR2GRAY)
            grayframe = grayframe[np.newaxis, ... ].astype(np.uint8)

            if n == 0:
                noisy_arrs = np.asarray(grayframe)
            else:
                noisy_arrs = np.append(noisy_arrs, grayframe, axis=0)

    return noisy_arrs


"""
### Read h264 format video. currently broken
def read_vid_h264():
    k = 0
    #incoming data
    for fileName in Path(imagePath).glob("*.h264"):
        vid_file = str(fileName)
        break
    vid = open(vid_file, 'rb')
    decoder = h264decoder.H264Decoder()
    while (1):
        data_in = vid.read(1024)
        if not data_in:
            break
        framedata, nread = decoder.decode_frame(data_in)
        data_in = data_in[nread:]
        (frame, width, height, lineSize) = framedata
        if frame is not None:
            frame = np.frombuffer(frame, dtype=np.ubyte, count=len(frame))
            frame = frame.reshape((height, lineSize//3, 3))
            frame = frame[:,:width,:]

            grayframe = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            grayframe = grayframe[np.newaxis, ... ].astype(np.uint8)

        if k == 0:
            noisy_arrs = np.asarray(grayframe)
        else:
            noisy_arrs = np.append(noisy_arrs, grayframe, axis=0)
        k += 1
        #print(noisy_arrs)
        if k == len(numberList):
            break

    return noisy_arrs

"""





### read relevant video frames from regular video, convert to grayscale and layer them
def read_vid_mkv():
    cap = cv.VideoCapture(vid_file_name)
    k = firstFrame
    ret = 1
    while ret:
        # set current frame to read
        frame_no = cap.set(cv.CAP_PROP_POS_FRAMES, k)

        ret, frame = cap.read()
        if ret != 1:
            break

        # grayscale
        grayframe = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # add axis to enable appending
        grayframe = grayframe[np.newaxis, ... ].astype(np.uint8)


        if k == firstFrame:
            # make the first array
            noisy_arrs = np.asarray(grayframe)

        else:
            # layer the current array on top of previous array
            noisy_arrs = np.append(noisy_arrs, grayframe, axis=0)

        k += 1
        if k == lastFrame:
            break

    return noisy_arrs




### Increase contrast by equalisizing histogram, without increasing noise
def adaptive_histogram_equalization(noisy_arrs):
    k = 0
    clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    for z in noisy_arrs:
        equalized = clahe.apply(z)
        equalized = equalized[np.newaxis, ...].astype(np.uint8)
        #print(equalized)

        if k == 0:
            # make the first array
            eq_arrs = np.asarray(equalized)

        else:
            # layer the current array on top of previous array
            eq_arrs = np.append(eq_arrs, equalized, axis=0)

        k += 1

    return eq_arrs




### Blur image to reduce noise. We don't really need sharp edges to estimate motion with findTransformECC()
def blurring(sharpImage):
    #blurredImage = cv.GaussianBlur(sharpImage, (3, 3), sigmaX=0)
    blurredImage = cv.bilateralFilter(sharpImage, 11, 20, 20)
    return blurredImage



### Denoise image numberIndex by comparing with other images in num_frames_window
def denoise_multi(arrays, numberIndex, num_frames_window):

    cleanImageArray = cv.fastNlMeansDenoisingMulti(srcImgs=arrays,
                        imgToDenoiseIndex=numberIndex, temporalWindowSize=num_frames_window,
                          h=10, templateWindowSize=17, searchWindowSize=41) # h is filter strength. h=10 is default
    return cleanImageArray





## Main function for denoising and blurring of image arrays
def denoising(eq_arrs):
    k = 0

    for z in eq_arrs:
        try: # Try, to enable error handling
            # denoise image
            if (k <= 1) or (k >= (len(numberList) - 3)):
                # denoise two first and last images individually
                cleanImageArray = cv.fastNlMeansDenoising(src=z,
                            h=15, templateWindowSize=17, searchWindowSize=41)
            elif (k <= 4) or (k >= (len(numberList) - 5)):
                # denoise using some neighbouring images as template
                cleanImageArray = denoise_multi(eq_arrs, k, 5)
            else:  #(numberIndex <= 7) or (numberIndex >= (len(numberList) - 7)):
                # denoise using more neighbouring images as template
                cleanImageArray = denoise_multi(eq_arrs, k, 9)
        except:
            print('something went wrong with denoising')
            break
        blurredImageArray = blurring(cleanImageArray) # blur to further reduce noise
        blurredImageArray = blurredImageArray[np.newaxis, ...].astype(np.uint8) # add axis to enable appending
        if k == 0:
            # make the first array
            blurred_arrs = np.asarray(blurredImageArray)
        if k != 0:
            # layer the current array on top of previous array
            blurred_arrs = np.append(blurred_arrs, blurredImageArray, axis=0)
        k += 1
        print(f'Frame {k} of {len(numberList)} Denoised')

    return blurred_arrs



hf5_params = dict(compression="gzip",
            compression_opts=9,
            shuffle=True)

def save_hf5(arrs, opt_timestamp_list, encoder_timestamp_list, encoder_pos_list):
    height, width, numbers = arrs.shape

    maxshape_imgs=(height, width, numbers+10)
    chunks_imgs = (height, width, 10)

    opt_ts_array = np.array(opt_timestamp_list)
    enc_ts_array = np.array(encoder_timestamp_list)[np.newaxis, ...]
    enc_pos_array = np.array(encoder_pos_list)[np.newaxis, ...]
    enc_data_array = np.append(enc_ts_array, enc_pos_array, axis=0)

    with h5py.File(imagePath + '/images.h5', 'w') as f:
         # create dataset with dimensions matching however many arrays were successfully processed.
         #Avoids issues with broadcasting arrays to dataset

        clean_dataset = f.create_dataset("clean_images", shape=(arrs.shape), maxshape=maxshape_imgs, chunks=chunks_imgs, dtype = 'uint8', **hf5_params)
        #set attributes for image dataset
        clean_dataset.attrs['CLASS'] = 'IMAGE'
        clean_dataset.attrs['IMAGE_VERSION'] = '1.2'
        clean_dataset.attrs['IMAGE_SUBCLASS'] =  'IMAGE_GRAYSCALE'
        clean_dataset.attrs['IMAGE_MINMAXRANGE'] = np.array([0,255], dtype=np.uint8)
        clean_dataset.attrs['IMAGE_WHITE_IS_ZERO'] =  0

        f['clean_images'].write_direct(arrs) #write all arrays at once. fast.


        # To facilitate the recomputation of already written data (using -c); it's necessary to save an identifier for each frame so that the corresponding timestamp can be determined.
        # Alternatively, save the timestamp itself.
        opt_timestamp_dataset = f.create_dataset("optical_timestamps", maxshape=(opt_ts_array.shape), shape=(opt_ts_array.shape), dtype = 'float32' ,**hf5_params)
        f['optical_timestamps'].write_direct(opt_ts_array)

        enc_dataset = f.create_dataset("encoder_data",  maxshape=(enc_data_array.shape), shape=(enc_data_array.shape), dtype = 'float32', **hf5_params)
        enc_dataset.attrs['orientation'] = 'TOP_IS_TIMESTAMP'

        f['encoder_data'].write_direct(enc_data_array)




### This function is not my original work, and I therefore have no authority to licence it. It's exempt from the general licence of the script. Taken from https://gist.github.com/crackwitz/31b3be85686ecbebb9d46cf525beaad5
def convolve_shift_calc(prevFrame, curFrame, normalize=True):
    assert prevFrame.shape == curFrame.shape
    (height, width) = prevFrame.shape[:2]
    if normalize:
        prevFrame = prevFrame.astype('f4') - prevFrame.mean()
        curFrame = curFrame.astype('f4') - curFrame.mean()
    scores = cv.filter2D(src=prevFrame, kernel=curFrame, ddepth=cv.CV_32F)
    (minVal, maxVal, minLoc, maxLoc) = cv.minMaxLoc(scores)
    shift = np.float32(maxLoc) - np.array([width, height])/2.0

    return shift




FB_opt_flow_params=dict(pyr_scale=.9,
                         levels=9,
                         winsize=42,
                         iterations=40,
                         poly_n=11,
                         poly_sigma=1.7,
                         flags=10)


### dense flow motion estimation. Uses all pixels in frame
def calculate_dense_flow(prevFrame, curFrame, input_flow):

    flow = cv.calcOpticalFlowFarneback(prevFrame, curFrame, input_flow, **FB_opt_flow_params)
    pdx, pdy = flow[..., 0], flow[..., 1]

    # showing distribution of motion values in flow, to aid with determining filter parameters
    #pdx_hist, pdx_bins = np.histogram(pdx, bins=np.linspace(np.min(pdx), np.max(pdx), 20)
    #fig = plt.figure(figsize =(10, 7))
    #plt.hist(pdx, bins=np.linspace(np.min(pdx), np.max(pdx), 20))
    #plt.title("dense optical flow x-axis motion Histogram")
    #plt.show()

    # Excluding outliers by using Inter-Quartile Range.
    # Because untextured filament has a higher percentage of apparent non-moving pixels,
    # the IQR is right-skewed to better account for the pixels that actually show motion
    q3x, q1x = np.percentile(pdx, [75,25])
    q3y, q1y = np.percentile(pdy, [75,25])
    IQRx, IQRy = q3x - q1x, q3y - q1y
    upper_bound_x, lower_bound_x = q3x + 1.25 * IQRx, q1x - 1 * IQRx
    upper_bound_y, lower_bound_y = q3y + 1.25 * IQRy, q1y - 1 * IQRy
    pdx = pdx[pdx < upper_bound_x]
    pdx = pdx[pdx > lower_bound_x]
    pdy = pdy[pdy < upper_bound_y]
    pdy = pdy[pdy > lower_bound_y]

    std_dev_x =np.std(pdx)
    std_dev_y =np.std(pdy)

    # taking the geometric mean of remaining inlier points
    pdx_average, pdy_average = np.mean(pdx), np.mean(pdy)

    return pdx_average, pdy_average, flow, IQRx, IQRy, std_dev_x, std_dev_y



# Get total shift in x- and y- direction between two image frames / arrays
# Most of this function is not my own work, and I therefore don't have licensing rights.
# It is excepted from the general licence of the script.
# Taken from: https://stackoverflow.com/questions/45997891/cv2-motion-euclidean-for-the-warp-mode-in-ecc-image-alignment-method/45998244#45998244
def calc_ECC_transform(prevFrame, curFrame, initial_transform_in):
    epsilon_ = 0.1
    Transform_ECC_params = dict(motionType = cv.MOTION_TRANSLATION, # only motion in x- and y- axes
                            criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 200,  epsilon_))

    # Construct scale pyramid to speed up and improve accuracy of transform estimation
    if height >= 180:
        nol = 11
    else:
        nol = 6 # number of layers
    numPasses = 2 # number of passes with gradually reduced epsilon
    init_warp = initial_transform_in

    prevFrame = [prevFrame]
    curFrame = [curFrame]
    for level in range(nol): # add resized layers to original array, to get 3 dimensions.
        prevFrame.insert(0, cv.resize(prevFrame[0], None, fx=0.8, fy=0.8,
                                   interpolation=cv.INTER_AREA))
        curFrame.insert(0, cv.resize(curFrame[0], None, fx=0.8, fy=0.8,
                                   interpolation=cv.INTER_AREA))

    # run pyramid ECC
    for passNumber in range(numPasses):

        ECCTransform_in = init_warp * np.array([[1, 1, 1.25], [1, 1, 1.25]], dtype=np.float32)**(1-nol) # adjust warp according to scale of array
        for level in range(nol):
            # Calculate the transform matrix which must be applied to prevFrame in order to match curFrame
            try:
                computedECC, ECCTransform = cv.findTransformECC(prevFrame[level], curFrame[level], ECCTransform_in, **Transform_ECC_params)
                measurement_flag = 1
                #print(f'ECCTransform: {ECCTransform} \ncc: {computedECC}\n\n')

            except:
                #print(f'\nECCTransform could not be found for layer {level+1} of {nol} on with epsilon {epsilon_}')
                ECCTransform = ECCTransform_in    # np.eye(2, 3, dtype=np.float32)
                #computedECC = 0
                measurement_flag = 0
            if level != nol-1:  # scale up for the next pyramid level, unless this layer is the original image
                ECCTransform_in = ECCTransform * np.array([[1, 1, 1.25], [1, 1, 1.25]], dtype=np.float32)

            if level == nol-1:
                init_warp = ECCTransform_in


        epsilon_ = epsilon_ * 0.1

    # Extract second element of first and second row, which is translation in their respective directions
    try:
        pdx, pdy = ECCTransform[0,2], ECCTransform[1,2]
    except:
        #print(f'no transform found for images')
        pdx = 0
        pdy = 0


    # I think computedECC is the confidence that the transform matrix fits.
    #print(f'\n\nECC confidence of transform: {computedECC}') #, \npixel delta x-axis: {pdx} \npixel delta y-axis:  {pdy}')

    return  pdx, pdy, measurement_flag





# These variables anchor motion estimates to real-world values
max_filament_speed = 140 # mm/min. However, under slip-stick conditions, this speed may be exceeded.
max_filament_speed_sec = max_filament_speed / 60 # mm/s
pixels_per_mm = 304 # estimated by counting pixels between edges of known object, in 640x480 mode
max_filament_speed = pixels_per_mm * max_filament_speed # pixels/second


# Instantiating stores for values
encoder_out_info_list = []
enc_pos_list = []
enc_ts_list = []
velocity_list_x = []
velocity_list_y = []
#motion_list_opt_ECC = []
out_information = []
csv_field_names = ['Timestamp [s]', 'mm/min X-axis optical', 'mm/min Y-axis optical']
tsList = [] # timestamps indexed per-frame
total_timestamp_list = [] # cumulative timestamps



### Read motion from encoder
def encoder_velocity():
    k = 2
    camera_triggered = 0
    filament_motion = 0
    filament_position_relative = 0
    total_timestamp_enc = 0
    total_timestamp_enc_relative = 0
    firstRunFlag = 1


    # get camera timestamp file
    for fileName in Path(imagePath).glob("*tstamps.txt"):
        tstamp_fileName = str(fileName)
        break
    firstFrameLine = linecache.getline(tstamp_fileName, firstFrame + 1)
    # fetch specific line at lastFrame index
    lastFrameLine = linecache.getline(tstamp_fileName, lastFrame + 1)


    first_timestamp_opt = float(firstFrameLine.split("\n")[0])
    first_timestamp_opt = first_timestamp_opt / 1000 # milliseconds to seconds
    # the maximum timestamp for optical estimation
    total_timestamp_opt = float(lastFrameLine.split("\n")[0])
    if total_timestamp_opt != '':
        total_timestamp_opt= total_timestamp_opt / 1000



    for fileName in Path(imagePath).glob("*encoder.csv"):
        encoder_log = str(fileName)
        break

    while camera_triggered == 0:
        k += 1
        line = linecache.getline(encoder_log, k)
        # The value showing whether extruder is active and therefore whether camera recording is triggered,
        # is at channel 9.
        camera_triggered = int(line.split(",")[8].split("\n")[0][0])


    while (total_timestamp_enc < first_timestamp_opt):
        line = linecache.getline(encoder_log, k)
        if line == "":
            break
        encoder_timestamp = float(line.split(",")[0]) # millisecond

        if firstRunFlag == 1:
            encoder_timestamp_second = encoder_timestamp / 1000
            old_ts = encoder_timestamp_second
            k += 1
            firstRunFlag = 0
            continue

        encoder_timestamp_second = encoder_timestamp / 1000 # millisecond to second
        timestamp_gap_enc = encoder_timestamp_second - old_ts
        total_timestamp_enc = total_timestamp_enc + timestamp_gap_enc
        old_ts = encoder_timestamp_second
        k += 1

    #total_timestamp_enc = 0
    firstRunFlag = 1


    while (total_timestamp_enc <= total_timestamp_opt):


        line = linecache.getline(encoder_log, k)
        if line == "":
            break
        encoder_timestamp = float(line.split(",")[0]) # millisecond
        filament_position = float(line.split(",")[2]) # mm


        if firstRunFlag == 1:
            encoder_timestamp_second = encoder_timestamp / 1000
            old_ts = encoder_timestamp_second
            old_pos = filament_position
            k += 1
            firstRunFlag = 0
            continue

        encoder_timestamp_second = encoder_timestamp / 1000 # millisecond to second
        timestamp_gap_enc = encoder_timestamp_second - old_ts
        total_timestamp_enc = total_timestamp_enc + timestamp_gap_enc
        total_timestamp_enc_relative = total_timestamp_enc_relative + timestamp_gap_enc

        filament_motion = filament_position - old_pos
        filament_position_relative += filament_motion
        velocity_encoder = (filament_motion / (timestamp_gap_enc/60)) # mm/min

        #print(f'\n\nencoder timestamp: {total_timestamp_enc_relative}, \nposition: {filament_position_relative}, \nvelocity: {velocity_encoder}')

        encoder_out_info = (total_timestamp_enc_relative, filament_position_relative, velocity_encoder)
        encoder_out_info_list.append(encoder_out_info)

        enc_pos_list.append(filament_position_relative)
        enc_ts_list.append(total_timestamp_enc_relative)

        old_ts = encoder_timestamp_second
        old_pos = filament_position
        k += 1



    return enc_pos_list, enc_ts_list




### Main process for motion and velocity estimation
def end_process(clean_arrs, ts_list=0):
    k = 0
    failed_estimates = 0
    total_motion_dense_flow = 0
    total_motion_ECCTransform = 0
    dense_motion_list = []
    ECC_motion_list = []
    velocity_list_dense = []
    if isinstance(ts_list, np.ndarray) == True:
        timestamp_k=0
    else:
        timestamp_k = firstFrame + 1 # skip lines with metadata and first (0.0 sec) timestamp
    old_vx = 0
    old_ts = 0
    current_timestamp = 0
    total_timestamp = 0
    timestamp_gap = 0
    ECC_lateral_position = 0
    ECC_perpendicular_position = 0
    dense_lateral_position = 0
    dense_perpendicular_position = 0
    pdx, pdy = 0, 0

     # get timestamp file
    for fileName in Path(imagePath).glob("*tstamps.txt"):
        tstamp_fileName = str(fileName)
        break

    # iterate over slice's first axis
    # breakpoint()
    for z in (clean_arrs):

        if k == 0:
            prevFrame = z
            #
            if isinstance(ts_list, np.ndarray) != True:
                line = linecache.getline(tstamp_fileName, timestamp_k)
                current_timestamp = float(line.split("\n")[0])
                timestamp_second = current_timestamp/1000
                #tsList.append(timestamp_second) # append to list of timestamps
                old_ts = timestamp_second
                timestamp_k += 1
                k += 1

            else:
                current_timestamp = ts_list[timestamp_k]
                old_ts = current_timestamp
                timestamp_k += 1
                k += 1

            continue # nothing to do with just the first image array


        else:
            # Camera timestamp handling
            if isinstance(ts_list, np.ndarray) != True:
                # fetch specific line from cached file,an efficient method.
                line = linecache.getline(tstamp_fileName, timestamp_k)

                current_timestamp = float(line.split("\n")[0]) # store the specific portion of the line as timestamp. microsecond format
                if current_timestamp == '':
                    current_timestamp = 1E-10

                timestamp_second = current_timestamp / (1000) # convert from millisecond to second

            else:
                # get timestamp from array
                total_timestamp = ts_list[timestamp_k]
                timestamp_second = total_timestamp
                timestamp_gap_s = total_timestamp - ts_list[timestamp_k - 1]


            timestamp_gap_s = timestamp_second - old_ts
            timestamp_gap_m = timestamp_gap_s / 60 # convert from second to minute
            total_timestamp += timestamp_gap_s
            old_ts = timestamp_second


            convolve_shift = convolve_shift_calc(prevFrame, z)


            # Dense optical flow motion estimation
            if k == 1:
                pdx_dense_flow, pdy_dense_flow, dense_flow, IQRx, IQRy, std_dev_x, std_dev_y = calculate_dense_flow(prevFrame, z, convolve_shift)
            else:
                pdx_dense_flow, pdy_dense_flow, dense_flow, IQRx, IQRy, std_dev_x, std_dev_y = calculate_dense_flow(prevFrame, z, dense_flow)

            total_motion_dense_flow += pdx_dense_flow  #convolve_shift[1]


            # ECCTransform motion estimation
            init_warp = np.eye(2, 3, dtype=np.float32)
            init_warp_prev_ECC = np.eye(2, 3, dtype=np.float32) # identity matrix
            init_warp_convolve = np.eye(2, 3, dtype=np.float32) # identity matrix
            init_warp_FB = np.eye(2, 3, dtype=np.float32) # identity matrix

            init_warp_prev_ECC[0,2], init_warp_prev_ECC[1,2] = pdx, pdy
            init_warp_convolve[0,2], init_warp_convolve[1,2] = convolve_shift[1], convolve_shift[0]
            init_warp_FB[0,2], init_warp_FB[1,2] = pdx_dense_flow, pdy_dense_flow

            pdx, pdy, measurement_flag = calc_ECC_transform(prevFrame, z, init_warp)

            if measurement_flag == 0:
                pdx, pdy, measurement_flag = calc_ECC_transform(prevFrame, z, init_warp_prev_ECC) # get pixel-relative motion between frames
            if measurement_flag == 0:
                pdx, pdy, measurement_flag = calc_ECC_transform(prevFrame, z, init_warp_FB) # get pixel-relative motion between frames
            if measurement_flag == 0:
                pdx, pdy, measurement_flag = calc_ECC_transform(prevFrame, z, init_warp_convolve) # get pixel-relative motion between frames

            total_motion_ECCTransform += pdx




            if measurement_flag == 0:
                failed_estimates += 1
                k += 1
                timestamp_k += 1
                continue


            tsList.append(total_timestamp) # append to list of timestamps
            #total_timestamp = total_timestamp + int(timestamp)
            #total_timestamp_list.append(total_timestamp)


            mm_dx, mm_dy = pdx / pixels_per_mm, pdy / pixels_per_mm # convert to millimeter-relative motion
            mm_dx_dense, mm_dy_dense = pdx_dense_flow / pixels_per_mm, pdy_dense_flow / pixels_per_mm # convert to millimeter-relative motion


            #converting from non-timebound relative motion to timebound (seconds) relative motion
            vxs, vys = mm_dx / timestamp_gap_s, mm_dy / timestamp_gap_s
            vxm, vym = mm_dx / timestamp_gap_m, mm_dy / timestamp_gap_m
            vxm_dense, vym_dense = mm_dx_dense / timestamp_gap_m, mm_dy_dense / timestamp_gap_m


            ECC_lateral_position += mm_dx
            ECC_perpendicular_position += mm_dy
            dense_lateral_position += mm_dx_dense
            dense_perpendicular_position += mm_dy_dense

            xmax = max_filament_speed * timestamp_gap_m # px/interval
            if k%300 == 0:
                print(f'frame {k} of {lastFrame-firstFrame}')
            #print(f'xmax = {xmax} pixels for this image interval. \npdx = {pdx} \npdy = {pdy}')


            velocity_list_x.append(vxm)
            velocity_list_y.append(vym)
            ECC_motion_list.append(ECC_lateral_position)
            dense_motion_list.append(dense_lateral_position)
            velocity_list_dense.append(vxm_dense)

            out_info = (total_timestamp, dense_lateral_position, vxm_dense, IQRx, std_dev_x, dense_perpendicular_position, vym_dense, IQRy, std_dev_y, ECC_lateral_position, vxm, ECC_perpendicular_position, vym)
            out_information.append(out_info)



# Implement a calculation of hypotenuse of motion. keep the data?

            prevFrame = z # store current array as different variable to use next iteration
            k += 1
            timestamp_k += 1



    print(f'\nfailed motion estimates: {failed_estimates} of {len(numberList)}')
    print(f'final Y position according to findTransformECC: {ECC_perpendicular_position}\n')
    print(f'final X position according to findTransformECC: {ECC_lateral_position}')
    print(f'final X position according to Dense optical flow: {dense_lateral_position}')



    return out_information, velocity_list_x, velocity_list_y, velocity_list_dense, tsList, ECC_motion_list, dense_motion_list



### Encoder data has uniform time intervals, camera data does not.
### This function resamples both datasets to timeseries with a common uniform time interval
def dataset_correlation(ECC_motion_list, dense_motion_list, optical_ts, encoder_pos, encoder_ts):
    k = 0
    interpolated_vel_list_opt_ECC = []
    interpolated_vel_list_opt_dense = []
    interpolated_vel_list_enc = []

    # interpolate data, passing through all original datapoints
    f_interpolated_opt_ECC_pos = interpolate.Akima1DInterpolator(optical_ts, ECC_motion_list)
    f_interpolated_opt_dense_pos = interpolate.Akima1DInterpolator(optical_ts, dense_motion_list)
    f_interpolated_enc_pos = interpolate.Akima1DInterpolator(encoder_ts, encoder_pos)

    # make new timestamp list, with equally spaced intervals and equal number of points as camera frames
    tsList_new = np.linspace(0, optical_ts[-1], len(optical_ts))

    # resample datasets to new timestamp list
    interpolated_opt_ECC_pos = f_interpolated_opt_ECC_pos(tsList_new)
    interpolated_opt_dense_pos = f_interpolated_opt_dense_pos(tsList_new)
    interpolated_enc_pos = f_interpolated_enc_pos(tsList_new)


    for mm_dx_opt_ECC, mm_dx_opt_dense, mm_dx_enc, ts in zip(interpolated_opt_ECC_pos, interpolated_opt_dense_pos, interpolated_enc_pos, tsList_new):
        if k == 0:
            old_ts = ts
            k += 1
            continue
        timestamp_gap = ts - old_ts
        v_opt_ECC = (mm_dx_opt_ECC / timestamp_gap) * 60
        v_opt_dense = (mm_dx_opt_dense / timestamp_gap) * 60
        v_enc = (mm_dx_enc / timestamp_gap) * 60

        interpolated_vel_list_opt_ECC.append(v_opt_ECC)
        interpolated_vel_list_opt_dense.append(v_opt_dense)
        interpolated_vel_list_enc.append(v_enc)

        k += 1

    return interpolated_opt_ECC_pos, interpolated_opt_dense_pos, interpolated_enc_pos, tsList_new, interpolated_vel_list_enc, interpolated_vel_list_opt_ECC, interpolated_vel_list_opt_dense



def correlate_stats_and_save(interpolated_vel_list_enc,
                       interpolated_vel_list_opt_ECC,
                       interpolated_vel_list_opt_dense,
                       out_information):

    ECC_vel_list = []
    dense_vel_list = []
    enc_vel_list = []
    diff_list_ECC=[]
    diff_list_dense=[]
    lateral_std_dev_list = []
    perpendicular_std_dev_list = []
    lateral_IQR_list = []
    perpendicular_IQR_list = []


    # gather velocity info
    k = 0
    for x, y, z in zip(interpolated_vel_list_enc,
                       interpolated_vel_list_opt_ECC,
                       interpolated_vel_list_opt_dense):
        if k < 2:
            k += 1
            continue
        if float(x) <= 0.01:
            continue

        diff_list_ECC.append(z-x)
        diff_list_dense.append(z-y)
        enc_vel_list.append(x)
        ECC_vel_list.append(y)
        dense_vel_list.append(z)

    if diff_list_ECC != []:
        mean_diff_ECC= np.mean(diff_list_ECC)
        mean_diff_dense = np.mean(diff_list_dense)
        abs_mean_diff_ECC = mean_absolute_error(enc_vel_list, ECC_vel_list)
        abs_mean_diff_dense = mean_absolute_error(enc_vel_list, dense_vel_list)
        mean_abs_percentage_error_ECC = mean_absolute_percentage_error(enc_vel_list, ECC_vel_list)
        mean_abs_percentage_error_dense = mean_absolute_percentage_error(enc_vel_list, dense_vel_list)


    # gather standard deviation and interquartile range
    k = 0
    for row in out_information:
        if k <2:
            k+=1
            continue
        lateral_IQR = float(row[3])
        lateral_std_dev = float(row[4])
        perpendicular_IQR = float(row[7])
        perpendicular_std_dev = float(row[8])

        lateral_std_dev_list.append(lateral_std_dev)
        perpendicular_std_dev_list.append(perpendicular_std_dev)
        lateral_IQR_list.append(lateral_IQR)
        perpendicular_IQR_list.append(perpendicular_IQR)

    lateral_IQR_mean = np.mean(lateral_IQR_list)
    perpendicular_IQR_mean = np.mean(perpendicular_IQR_list)
    lateral_std_dev_mean = np.mean(lateral_std_dev_list)
    perpendicular_std_dev_mean = np.mean(perpendicular_std_dev_list)

    if enc_vel_list != []:
        #calculate Spearman Rank correlation and corresponding p-value
        rho_ecc, p_ecc = spearmanr(ECC_vel_list, enc_vel_list)
        rho_dense, p_dense = spearmanr(dense_vel_list, enc_vel_list)

        with open(imagePath + '/stats.txt', 'w') as txt:
            txt.write(f'{lateral_std_dev_mean} is lateral flow std.dev mean\n\
{perpendicular_std_dev_mean} is perpendicular flow std.dev mean\n\n\
{lateral_IQR_mean} is lateral flow IQR mean\n\
{perpendicular_IQR_mean} is perpendicular flow IQR mean\n\n\
{rho_ecc} is Spearman rank correlation for ECC and \n\
{p_ecc} is associated p-value\n\
{rho_dense} is Spearman rank correlation for dense and \n\
{p_dense} is associated p-value\n\n\
{mean_diff_ECC} is mean velocity deviation from encoder for ECC, mm/min\n\
{mean_diff_dense} is mean velocity deviation from encoder for dense flow, mm/min\n\n\
{abs_mean_diff_ECC} is mean absolute velocity deviation from encoder for ECC, mm/min\n\
{abs_mean_diff_dense} is mean absolute velocity deviation from encoder for dense, mm/min\n\n\
{mean_abs_percentage_error_ECC} is mean absolute velocity deviation from encoder for ECC, percentage\n\
{mean_abs_percentage_error_dense} is mean absolute velocity deviation from encoder for dense, percentage\n')




def presentData(out_information,
                tsList,
                interpolated_opt_ECC_pos,
                interpolated_opt_dense_pos,
                interpolated_enc_pos,
                interpolated_tsList,
                interpolated_vel_list_enc,
                interpolated_vel_list_opt_ECC,
                interpolated_vel_list_opt_dense):


    optical_data_csv_headers = ['timestamp [s]', 'dense lateral position [mm]', 'dense lateral velocity [mm/min]', 'interquartile range lateral dense motion field', 'std.dev. lateral dense motion field', 'dense perpendicular position', 'dense perpendicular velocity [mm/min]', 'interquartile range perpendicular dense motion field', 'std.dev. perpendicular dense motion field', 'ECC lateral position [mm]', 'ECC lateral velocity [mm/min]', 'ECC perpendicular position', 'ECC perpendicular velocity']

    correlated_position_csv_headers = ['timestamp [s]', 'position optical [ECC] [mm]', 'position optical [dense]', 'position encoder', 'velocity ECC', 'velocity dense', 'velocity encoder']

    ### write comma separated value file, for reuse in other software or analysis
    with open(imagePath + '/optical_estimates.csv', 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(optical_data_csv_headers)
        csvwriter.writerows(out_information)

    with open(imagePath + '/correlated_data.csv', 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(correlated_position_csv_headers)
        for value in zip(interpolated_tsList, interpolated_opt_ECC_pos, interpolated_opt_dense_pos, interpolated_enc_pos, interpolated_vel_list_opt_ECC, interpolated_vel_list_opt_dense, interpolated_vel_list_enc):
            csvwriter.writerow(value)


# having many datapoints begets high resolution graphs

    fig1 = plt.figure(figsize=(100,40))
    plt.plot(interpolated_tsList, interpolated_enc_pos, c = 'red', marker = 'o', linewidth='4')
    plt.plot(interpolated_tsList, interpolated_opt_ECC_pos, c = 'blue', marker = 'o', linewidth='4')
    plt.plot(interpolated_tsList, interpolated_opt_dense_pos, c = 'green', marker = 'o', linewidth='4')
    plt.grid(color='green', linestyle='-')
    plt.xlabel('timestamp seconds. Blue is optical [ECC], Green is optical [dense], red is encoder', fontsize=32)
    plt.ylabel('lateral position [mm]', fontsize=32)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)
    fig1.savefig(fname = (f'{imagePath}/enc+opt_lateral_position_frames{firstFrame}-{lastFrame}.png'), dpi =100)
    #plt.show()



    fig1 = plt.figure(figsize=(100,40))
    plt.plot(interpolated_tsList[1:], interpolated_vel_list_enc, c = 'red', marker = 'o', linewidth='4')
    plt.plot(interpolated_tsList[1:], interpolated_vel_list_opt_ECC, c = 'blue', marker = 'o', linewidth='4')
    plt.plot(interpolated_tsList[1:], interpolated_vel_list_opt_dense, c = 'green', marker = 'o', linewidth='4')
    plt.grid(color='green', linestyle='-')
    plt.xlabel('timestamp in seconds. Blue is optical [ECC], Green is optical [dense], red is encoder', fontsize=32)
    plt.ylabel('lateral velocity [mm/min]', fontsize=32)
    plt.xticks(fontsize=24)
    plt.yticks(fontsize=24)
    fig1.savefig(fname = (f'{imagePath}/enc+opt_vel_frames{firstFrame}-{lastFrame}.png'), dpi =100)
    #plt.show()



def main():
    if continuation != 'True':
        # get input
        if use_h264 == "True":
            noisy_arrs = read_vid_h264()

        elif use_raw == "True":
            headedList = checkRawHeader()
            noisy_arrs = readRaws(headedList)
        else:
            noisy_arrs = read_vid_mkv()

        # enhance contrast
        clean_arrs = adaptive_histogram_equalization(noisy_arrs)

        if use_denoise == "True":
            # denoise images
            clean_arrs = denoising(clean_arrs)


        # read encoder motion and timestamps from csv file
        enc_pos_list, enc_ts_list = encoder_velocity()

        # find velocity
        out_information, ECC_velocity_list_x, ECC_velocity_list_y, velocity_list_dense, opt_ts_list, ECC_motion_list, dense_motion_list = end_process(clean_arrs)
        print(f'final X position according to Encoder: {enc_pos_list[-1]}\n')

        save_hf5(clean_arrs, opt_ts_list, enc_ts_list, enc_pos_list)

    else:
        with h5py.File(imagePath + '/images.h5', 'r') as f:
            clean_arrs = f['clean_images'][()]
            ts_list = f['optical_timestamps'][()]
            encoder_data = f['encoder_data'][()]
            enc_pos_list, enc_ts_list = encoder_data[0], encoder_data[1]




        out_information, ECC_velocity_list_x, ECC_velocity_list_y, velocity_list_dense, opt_ts_list, ECC_motion_list, dense_motion_list = end_process(clean_arrs, ts_list)


    interpolated_opt_ECC_pos, interpolated_opt_dense_pos, interpolated_enc_pos, interpolated_tsList, interpolated_vel_list_enc, interpolated_vel_list_opt_ECC, interpolated_vel_list_opt_dense = dataset_correlation(ECC_motion_list,                                                                                                                                                                                                                     dense_motion_list,
    opt_ts_list,
    enc_pos_list,
    enc_ts_list)

    correlate_stats_and_save(interpolated_vel_list_enc,
                             interpolated_vel_list_opt_ECC,
                             interpolated_vel_list_opt_dense,
                             out_information)


    run_time = round(((time.time() - start_time)/60), 2)
    print(f'total runtime is {run_time} minutes')

    # present data
    presentData(out_information,
                tsList,
                interpolated_opt_ECC_pos,
                interpolated_opt_dense_pos,
                interpolated_enc_pos,
                interpolated_tsList,
                interpolated_vel_list_enc,
                interpolated_vel_list_opt_ECC,
                interpolated_vel_list_opt_dense)


if __name__ == "__main__":
    main()



