# motion-estimation

Public repository for the software developed to analyze data gathered from experiments in the thesis http://dx.doi.org/10.13140/RG.2.2.17279.69285.
The software estimates full-frame motion between images in a video, then determines velocity according to encoder positions and aforementioned optical estimates. These velocity datasets are compared statistically and visualized.

## Supported optical input data:
- Videofiles in all formats supported by OpenCV.
- Raw image series in a folder (when raw image headers are included in raw files, or a path to a header file is specified).
- .h264 videos without container (broken for now).

## Other required data:
- CSV file of timestamps and position of encoder (either directly synced with optical timestamps or with a "telltale" column which can be used to sync optical and encoder timestamps).
- CSV or txt file with timestamps or timestamp gaps for optical measurement.

## Output:
- hdf5 file containing processed images, synced timestamps, encoder positions.
- CSV file containing synced and interpolated timestamps, positions and velocities for optical and encoder measurements.
- Two graphs showing interpolated encoder and optical positions and velocities.


## Environment
This software is confirmed able to run on the following platforms:
- Ubuntu 22.04 PRoot environment through Termux on Android 12.
- Ubuntu 20.04 container on Fedora Linux 36.

